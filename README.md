<!--suppress HtmlRequiredAltAttribute -->
<a href="https://www.ensicaen.fr">
<img src="https://www.ensicaen.fr/wp-content/uploads/2017/02/LogoEnsicaen.gif" width="256" >
</a>

PYTHON PROJECT : SMART TORTOISE
=============================================
Ludovic THAI - ludovic.thai@ecole.ensicaen.fr \
Benjamin VIGNAUX - bvignaux@ecole.ensiaen.fr

# Random agent


~~~
./tortoise.py
python3 tortoise.py -a RandomBrain -w 15 -s 40
~~~

## ReflexAgent

~~~
./tortoise.py -a ReflexBrain -w 15 -s 30
~~~

## Exercise: Goald Based Agent"

~~~
./tortoise.py -a GoalBasedBrain -w 15 -s 30

./runs.py -a GoalBasedBrain -w 15 -n 10
