# -*- coding: utf-8; mode: python -*-

# ENSICAEN
# École Nationale Supérieure d'Ingénieurs de Caen
# 6 Boulevard Maréchal Juin
# F-14050 Caen Cedex France
#
# Artificial Intelligence 2I1AE1

# @file agents.py
#
# @author Régis Clouard
# @author Ludovic Thai
# @author Benjamin Vignaux

import random

# Useful constants
EAT = 'eat'
DRINK = 'drink'
FORWARD = 'forward'
LEFT = 'left'
RIGHT = 'right'
WAIT = 'wait'

DIRECTIONTABLE = [(0, -1), (1, 0), (0, 1), (-1, 0)] # North, East, South, West

class TortoiseBrain:
    """
    The base class for various flavors of the tortoise brain.
    This an implementation of the Strategy design pattern.
    """
    def think( self, sensor ):
        raise Exception("Invalid Brain class, think() not implemented")

class RandomBrain( TortoiseBrain ):
    """
    An example of simple tortoise brain: acts randomly...
    """
    def init( self, grid_size ):
        pass

    def think( self, sensor ):
        return random.choice([EAT, DRINK, LEFT, RIGHT, FORWARD, FORWARD, WAIT])

class ReflexBrain( TortoiseBrain ):
    def init( self, grid_size ):
        pass

    def think( self, sensor ):
        # case 1: danger: dog
        if abs(sensor.dog_front) < 3 and abs(sensor.dog_right) < 3:
            if sensor.dog_front <= 0:
                if sensor.free_ahead:
                    return FORWARD;
                elif sensor.dog_right > 0:
                    return LEFT
                else:
                    return RIGHT
            elif sensor.dog_front > 0:
                if sensor.dog_right > 0:
                    return LEFT;
                else:
                    return RIGHT
        # increase the performance measure
        if sensor.lettuce_here and sensor.drink_level > 10: return EAT
        if sensor.water_ahead and sensor.drink_level < 50: return FORWARD
        if sensor.water_here and sensor.drink_level < 100: return DRINK
        # Nothing to do: move
        if sensor.free_ahead:
            return random.choice([FORWARD, RIGHT, FORWARD, WAIT,
                                  FORWARD, FORWARD, FORWARD])
        else:
            return random.choice([RIGHT, LEFT])
        return random.choice([EAT, DRINK, LEFT, RIGHT, FORWARD, FORWARD, WAIT])

 #  ______                               _              
 # |  ____|                             (_)             
 # | |__    __  __   ___   _ __    ___   _   ___    ___ 
 # |  __|   \ \/ /  / _ \ | '__|  / __| | | / __|  / _ \
 # | |____   >  <  |  __/ | |    | (__  | | \__ \ |  __/
 # |______| /_/\_\  \___| |_|     \___| |_| |___/  \___|

WATER_LIMIT = 55

class GoalBasedBrain( TortoiseBrain ):

    map = []
    
    def init(self, grid_size):
        self.initialize_map(grid_size)
        pass

    def think(self, sensor):
        """
        Returns the best action with regard to the current state of the game.
        Available actions are [EAT, DRINK, LEFT, RIGHT, FORWARD, WAIT].

        sensors attributes:
        sensor.free_ahead: there is no stone or wall one step ahead (boolean).
        sensor.lettuce_ahead: there is a lettuce plant one step ahead (boolean).
        sensor.lettuce_here: there is a lettuce plant at the current position (boolean).
        sensor.water_ahead: there is water one step ahead (boolean).
        sensor.water_here :there is water at the current position (boolean).
        sensor.drink_level : the level of water in the tortoise’s body, ranging from 100 to 0.
        sensor.health_level: the level of health in the tortoise’s body, ranging from 100 to 0.
        sensor.dog_front: the relative position of the dog, ie. the number of cells in front (positive) or behind (negative) the tortoise that it is.
        sensor.dog_right: the relative position of the dog to the right, ie. the number of cells to the right (positive) or left (negative) of the tortoise that it is.
        sensor.tortoise_position: the tortoise coordinates (x,y).
        sensor.tortoise_direction: the tortoise direction between 0 (north), 1 (east), 2 (south), and 3 (west).


        Compute the tortoise direction (dx,dy) in the grid from the sensor absolute direction.
        e.g: North -> (0, -1); South -> (0, 1)
        (dx, dy) = DIRECTIONTABLE[sensor.tortoise_direction]

        Compute the coordinates of the dog from the tortoise direction.
        if directionx == 0:
            self.dogx = self.x - directiony * sensor.dog_right
            self.dogy = self.y + directiony * sensor.dog_front
        else:
            self.dogx = self.x + directionx * sensor.dog_front
            self.dogy = self.y + directionx * sensor.dog_right
        """
        self.update_state(sensor)
        self.display_map()
        if sensor.lettuce_here:
            return EAT
        elif sensor.drink_level < 90 and sensor.water_here:
            return DRINK
        elif sensor.drink_level < WATER_LIMIT:
            (posX, posY) = self.search('W', sensor.tortoise_position)
        else:
            (posX, posY) = self.search('L', sensor.tortoise_position)

        if (posX, posY) == (-1, -1):
            (posX, posY) = self.search('-', sensor.tortoise_position)

        """
        The path returns by A* to go to the desired position.
        """
        path = self.a_star(sensor, sensor.tortoise_position,
                           sensor.tortoise_direction, (posX, posY))
        """
        We take the node after the started node.
        """
        nextNode = path[1]

        """
        If the next node has the same position than the tortoise, we have to 
        determine the side we need to turn.
        """
        if sensor.tortoise_position == nextNode.position:
            if nextNode.direction == (sensor.tortoise_direction+1) % 4:
                return RIGHT
            else:
                return LEFT
        else:
            return FORWARD

    def search(self, char, currentPos):
        """
        Returns the position of the nearest given char in the map.
        """
        distanceMin = 1e20
        (posX, posY) = (-1, -1)

        for i in range(len(self.map)):
            for j in range(len(self.map)):
                if self.map[i][j] == char:
                    distance = (currentPos[0] - i) ** 2 + \
                               (currentPos[1] - j) ** 2
                    if distance < distanceMin:
                        distanceMin = distance
                        (posX, posY) = (i, j)

        return posX, posY

    def initialize_map(self, grid_size):
        """
        Initializes the map.
            '#' for walls
            '-' for unknown boxes
        """
        self.map.clear()
        for i in range(grid_size):
            self.map.append(['-'] * grid_size)
        for i in range(grid_size):
            self.map[0][i] = '#'
            self.map[grid_size - 1][i] = '#'
            self.map[i][0] = '#'
            self.map[i][grid_size - 1] = '#'
        
    def display_map(self):
        """
        Displays the map known by the tortoise.
        """
        for i in range(len(self.map)):
            s = ""
            for j in range(len(self.map)):
                s = s + str(self.map[j][i]) + ' '
            print(s)
        print()

    def update_state(self, sensor):
        """
        Update the map when the tortoise move.
            '#' for rocks
            'W' for water
            'L' for lettuce
            '.' for empty case
        """
        (pos_x, pos_y) = sensor.tortoise_position
        (dx, dy) = DIRECTIONTABLE[sensor.tortoise_direction]

        if self.map[pos_x][pos_y] != 0:
            if sensor.water_here:
                self.map[pos_x][pos_y] = 'W'
            elif sensor.lettuce_here:
                self.map[pos_x][pos_y] = 'L'
            else:
                self.map[pos_x][pos_y] = '.'

        if sensor.water_ahead:
            self.map[pos_x + dx][pos_y + dy] = 'W'
        elif sensor.lettuce_ahead:
            self.map[pos_x + dx][pos_y + dy] = 'L'
        elif not sensor.free_ahead:
            self.map[pos_x + dx][pos_y + dy] = '#'

    def a_star(self, sensor, startPos, startDir, endPos):
        """
        Find the shortest path to go to a designated point using an heuristic
        Heuristic: distance between the node and the final node and the
        distance between the node and the dog.
        """
        startNode = Node(None, startPos, startDir)
        endNodes = [Node(None, endPos, 0), Node(None, endPos, 1),
                    Node(None, endPos, 2), Node(None, endPos, 3)]

        openList = []
        closedList = []

        openList.append(startNode)

        while len(openList) > 0:
            currentNode = openList[0]
            currentIndex = 0

            for index, node in enumerate(openList):
                if node.f < currentNode.f:
                    currentNode = node
                    currentIndex = index

            openList.pop(currentIndex)
            closedList.append(currentNode)

            if currentNode in endNodes:
                path = []
                current = currentNode
                while current is not None:
                    path.append(current)
                    current = current.parent
                return path[::-1]

            nodes = currentNode.getNextNodes(self.map)

            for node in nodes:
                if node in closedList:
                    continue

                node.g = currentNode.g + 1

                coef = 25
                (dx, dy) = DIRECTIONTABLE[startDir]
                if dx == 0:
                    dogx = startPos[0] - dy * sensor.dog_right
                    dogy = startPos[1] + dy * sensor.dog_front
                else:
                    dogx = startPos[0] + dx * sensor.dog_front
                    dogy = startPos[1] + dx * sensor.dog_right

                node.h = ((node.position[0] - endNodes[0].position[0]) ** 2) + (
                            (node.position[1] - endNodes[0].position[1]) **
                            2) + coef / (((node.position[0] - dogx) ** 2) + (
                            (node.position[1] - dogy) ** 2) + 1)
                node.f = node.g + node.h

                for openNode in openList:
                    if node == openNode and node.g > openNode.g:
                        continue

                openList.append(node)


class Node:
    """
    Define a state in Astar using a position, a direction and the previous 
    state.
    """
    def __init__(self, parent=None, position=None, direction=None):
        self.parent = parent
        self.position = position
        self.direction = direction

        self.g = 0
        self.h = 0
        self.f = 0

    def __eq__(self, other):
        return self.position == other.position and self.direction == \
               other.direction

    def getNextNodes(self, state):
        """
        Returns all the possible states after a given state.
        """
        nextNodes = [Node(self, self.position,
                          (self.direction + 1) % 4),
                     Node(self, self.position,
                          (self.direction - 1) % 4)]

        (dx, dy) = DIRECTIONTABLE[self.direction]
        newPosx = self.position[0] + dx
        newPosy = self.position[1] + dy

        if state[newPosx][newPosy] != '#':
            nextNodes.append(Node(self, (newPosx, newPosy),
                                      self.direction))

        return nextNodes
